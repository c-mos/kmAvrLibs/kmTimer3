/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmTimer3.c
*
*  **Created on**: May 03, 2022 @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  kmDebug library for AVR MCUs
*  Copyright (C) 2019  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include "../kmCommon/kmCommon.h"
#ifdef TIMSK3
#include "kmTimer3.h"

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#include "../kmTimersCommon/kmTimer3Defs.h"


// Definitions
#define KM_TCC3_TOP KM_TCC_TOP_3

#define KM_TCC3_CYCLES_CALCULATION_CORRECTION 1u

// "Private" variables
static uint8_t _Timer3PrescalerSelectBits = 0;

static uint16_t _Timer3CompOvfCycles = KM_TCC3_TOP;

static uint16_t _Timer3CompACycles = KM_TCC3_TOP;
static bool _Timer3OtputComparePinUsedA = false;

static uint16_t _Timer3CompBCycles = KM_TCC3_TOP;
static bool _Timer3OtputComparePinUsedB = false;

#ifdef COM3C0
static uint16_t _Timer3CompCCycles = KM_TCC3_TOP;
static bool _Timer3OtputComparePinUsedC = false;
#endif /* COM3C0 */

static kmTimer3CallbackType *_timer3CallbackOVF = NULL;
static void *_Timer3CallbackUserDataOVF = NULL;

static kmTimer3CallbackType *_timer3CallbackCompA = NULL;
static void *_Timer3CallbackUserDataCompA = NULL;

static kmTimer3CallbackType *_timer3CallbackCompB = NULL;
static void *_Timer3CallbackUserDataCompB = NULL;

#ifdef COM3C0
static kmTimer3CallbackType *_timer3CallbackCompC = NULL;
static void *_Timer3CallbackUserDataCompC = NULL;
#endif /* COM3C0 */

static kmTimer3InpCaptureCallbackType *_timer3CallbackInpCapture = NULL;
static void *_Timer3CallbackUserDataInpCapture = NULL;
static uint16_t _inpCaptureCycles = 0;

// "Private" functions

// "Public" functions
// Controlling timer flow
void kmTimer3Start(void);
void kmTimer3Stop(void);
void kmTimer3Restart(void);

// Controlling PWM
void kmTimer3SetPwmDutyBottomToTop(Tcc3PwmOut pwmOut, uint16_t duty);
void kmTimer3SetPwmInversion(Tcc3PwmOut pwmOut, bool inverted);

// Timer flow calculations
uint16_t kmTimer3CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);
uint16_t kmTimer3CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);
uint16_t kmTimer3CalcDutyOnCycles(uint16_t duty, uint16_t cycles);

// Implementation
void kmTimer3Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB) {
	kmTimer3Stop();
	_Timer3PrescalerSelectBits = prescaler;
	TCCR3A = (TCCR3A & ~KM_TCC3_MODE_MASK_A) | modeA;
	TCCR3B = (TCCR3B & ~KM_TCC3_MODE_MASK_B) | modeB;
}

void kmTimer3InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler) {
	kmTimer3Init(prescaler, KM_TCC3_MODE_0_A, KM_TCC3_MODE_0_B);
}

void kmTimer3InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds) {
	kmTimer3InitOnAccurateTimeCompAInterruptCallback(periodInMicroseconds >> KMC_DIV_BY_2, true);
}

void kmTimer3InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC3A) {
	_Timer3CompACycles = kmTimer3CalcPerdiod(microseconds, &_Timer3PrescalerSelectBits);
	kmTimer3Init(_Timer3PrescalerSelectBits, KM_TCC3_MODE_4_A, KM_TCC3_MODE_4_B);
	kmTimer3SetValueCompA(_Timer3CompACycles);
	kmTimer3ConfigureOCA(outOnOC3A ? KM_TCC3_A_PWM_COMP_OUT_TOGGLE : KM_TCC3_A_PWM_COMP_OUT_NORMAL);
}

void kmTimer3InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB) {
	_Timer3CompACycles = kmTimer3CalcPerdiod(microseconds, &_Timer3PrescalerSelectBits);
	_Timer3CompBCycles = kmTimer3CalcDutyOnCycles(phaseIntB, _Timer3CompACycles);
	kmTimer3Init(_Timer3PrescalerSelectBits, KM_TCC3_MODE_4_A, KM_TCC3_MODE_4_B);
	kmTimer3SetValueCompA(_Timer3CompACycles);
	kmTimer3SetValueCompB(_Timer3CompBCycles);
}

void kmTimer3InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer3Init(prescaler, KM_TCC3_MODE_E_A, KM_TCC3_MODE_E_B);
	kmTimer3SetValueOvf(_Timer3CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		kmTimer3ConfigureOCA(invertedA ? KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer3SetValueCompA(dutyA);
	}
}

void kmTimer3InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	kmTimer3InitOnPrescalerBottomToTopFastPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > KMC_UNSIGNED_ZERO) {
		kmTimer3ConfigureOCB(invertedB ? KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer3SetValueCompB(dutyB);
	}

#ifdef COM3C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		kmTimer3ConfigureOCC(invertedC ? KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer3SetValueCompC(dutyC);
	}
#endif /* COM3C0 */
}

uint16_t kmTimer3InitOnAccurateTimeFastPwm(const uint32_t microseconds
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	
	_Timer3CompOvfCycles = kmTimer3CalcPerdiodWithMinPwmAccuracy(microseconds, &_Timer3PrescalerSelectBits, KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer3Init(_Timer3PrescalerSelectBits, KM_TCC3_MODE_E_A, KM_TCC3_MODE_E_B);
	kmTimer3SetValueOvf(_Timer3CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		_Timer3CompACycles = kmTimer3CalcDutyOnCycles(dutyA, _Timer3CompOvfCycles);
		kmTimer3SetValueCompA(_Timer3CompACycles);
		kmTimer3ConfigureOCA(invertedA ? KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_Timer3CompBCycles = kmTimer3CalcDutyOnCycles(dutyB, _Timer3CompOvfCycles);
		kmTimer3SetValueCompB(_Timer3CompBCycles);
		kmTimer3ConfigureOCB(invertedB ? KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM3C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_Timer3CompCCycles = kmTimer3CalcDutyOnCycles(dutyC, _Timer3CompOvfCycles);
		kmTimer3SetValueCompC(_Timer3CompCCycles);
		kmTimer3ConfigureOCC(invertedC ? KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM3C0 */

	return _Timer3CompOvfCycles;
}

void kmTimer3InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer3Init(prescaler, KM_TCC3_MODE_A_A, KM_TCC3_MODE_A_B);
	kmTimer3SetValueOvf(_Timer3CompOvfCycles);

	kmTimer3SetValueCompA(dutyA);
	kmTimer3ConfigureOCA(invertedA ? KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}

void kmTimer3InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	kmTimer3InitOnPrescalerBottomToTopPcPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > 0) {
		kmTimer3SetValueCompB(dutyB);
		kmTimer3ConfigureOCB(invertedB ? KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM3C0
	if (dutyC > 0) {
		kmTimer3SetValueCompC(dutyC);
		kmTimer3ConfigureOCC(invertedC ? KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM3C0 */
}

uint16_t kmTimer3InitOnAccurateTimePcPwm(const uint32_t microseconds
										, const uint16_t dutyA
										, const bool invertedA
										, const uint16_t dutyB
										, const bool invertedB
#ifdef COM3C0
										, const uint16_t dutyC
										, const bool invertedC
#endif /* COM3C0 */
										) {
	_Timer3CompOvfCycles = kmTimer3CalcPerdiodWithMinPwmAccuracy(microseconds >> KMC_DIV_BY_2, &_Timer3PrescalerSelectBits, KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer3SetValueOvf(_Timer3CompOvfCycles);
	kmTimer3Init(_Timer3PrescalerSelectBits, KM_TCC3_MODE_A_A, KM_TCC3_MODE_A_B);
	
	if (dutyB > KMC_UNSIGNED_ZERO) {
		_Timer3CompACycles = kmTimer3CalcDutyOnCycles(dutyA, _Timer3CompOvfCycles);
		kmTimer3SetValueCompA(_Timer3CompACycles);
		kmTimer3ConfigureOCA(invertedB ? KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_Timer3CompBCycles = kmTimer3CalcDutyOnCycles(dutyB, _Timer3CompOvfCycles);
		kmTimer3SetValueCompB(_Timer3CompBCycles);
		kmTimer3ConfigureOCB(invertedB ? KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM3C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_Timer3CompCCycles = kmTimer3CalcDutyOnCycles(dutyC, _Timer3CompOvfCycles);
		kmTimer3SetValueCompC(_Timer3CompCCycles);
		kmTimer3ConfigureOCC(invertedC ? KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM3C0 */

	return _Timer3CompOvfCycles;
}

void kmTimer3InitExternal(bool falling) {
	if (true == falling) {
		_Timer3PrescalerSelectBits = KM_TCC3_EXT_T3_FAL;
		} else {
		_Timer3PrescalerSelectBits = KM_TCC3_EXT_T3_RIS;
	}
}

void kmTimer3InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer3CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer3InpCaptureCallbackType *inpCaptureCallback) {
	KM_TCC3_PWM_DDR &= ~_BV(KM_TCC3_T3_PIN);
	KM_TCC3_PWM_PORT |= _BV(KM_TCC3_T3_PIN);

	kmTimer3InitOnAccurateTimeCompAInterruptCallback(iddleTimeMicroseconds, false);

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(iddleUserData), iddleCallback);
	kmTimer3RegisterCallbackInpCapture(KM_TIMER3_USER_DATA(inpCaptureUserData), inpCaptureCallback);
	kmTimer3EnableInterruptCompA();
	kmTimer3EnableInterruptInpCapture();

	kmTimer3Start();
}

uint16_t kmTimer3CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange) {
	uint32_t result = ((uint32_t)cyclesRange * (uint32_t)duty) / (uint32_t)KM_TCC3_TOP;
	return (uint16_t)result;
}

void kmTimer3SetPwmDutyAccurateTimeModes(uint16_t duty) {
	kmTimer3SetValueCompB(kmTimer3CalcDutyOnCycles(duty, _Timer3CompACycles));
}

void kmTimer3SetPwmDutyBottomToTop(Tcc3PwmOut pwmOut, uint16_t duty) {
	switch (pwmOut) {
		case KM_TCC3_PWM_OUT_A: {
			kmTimer3SetValueCompA(duty);
			break;
		}
		case KM_TCC3_PWM_OUT_B: {
			kmTimer3SetValueCompB(duty);
			break;
		}
#ifdef COM3C0
		case KM_TCC3_PWM_OUT_C: {
			kmTimer3SetValueCompC(duty);
			break;
		}
#endif /* COM3C0 */
		// no default
	}
}

uint16_t kmTimer3CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler) {
	uint64_t cycles = (int64_t)(F_CPU);
	cycles *= microseconds;
	cycles /= KMC_CONV_MICROSECONDS_TO_SECONCS;
	if (cycles <= KM_TCC3_TOP) {
		// no prescaler, full XTAL
		*prescaler = KM_TCC3_PRSC_1;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC3_TOP) {
		// prescaler by /8
		*prescaler = KM_TCC3_PRSC_8;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC3_TOP) {
		// prescaler by /64
		*prescaler = KM_TCC3_PRSC_64;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC3_TOP) {
		// prescaler by /256
		*prescaler = KM_TCC3_PRSC_256;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC3_TOP) {
		// prescaler by /1024
		*prescaler = KM_TCC3_PRSC_1024;
	} else {
		// request was out of bounds, set as maximum
		*prescaler = KM_TCC3_PRSC_1024;
		cycles = KM_TCC3_TOP;
	}
	return (uint16_t)(cycles != KMC_UNSIGNED_ZERO ? cycles - KM_TCC3_CYCLES_CALCULATION_CORRECTION : cycles);
}

uint16_t kmTimer3CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy) {
	uint16_t result = kmTimer3CalcPerdiod(microseconds, prescaler);
	
	if (result < minimumCycleAcuracy) {
		result = minimumCycleAcuracy;
	}
	
	return result;
}

void kmTimer3SetPwmInversion(Tcc3PwmOut pwmOut, bool inverted) {
	switch (pwmOut) {
		case KM_TCC3_PWM_OUT_A: {
			if (false == inverted) {
				kmTimer3ConfigureOCA(KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer3ConfigureOCA(KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
		case KM_TCC3_PWM_OUT_B: {
			if (false == inverted) {
				kmTimer3ConfigureOCB(KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer3ConfigureOCB(KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#ifdef COM3C0
		case KM_TCC3_PWM_OUT_C: {
			if (false == inverted) {
				kmTimer3ConfigureOCC(KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer3ConfigureOCC(KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#endif /* COM3C0 */
		default: {
			// intentionally
		}
	}
}

void kmTimer3Start(void) {
	TCCR3B = (TCCR3B & ~KM_TCC3_CS_MASK) | _Timer3PrescalerSelectBits;
}

void kmTimer3Stop(void) {
	TCCR3B &= (TCCR3B & ~KM_TCC3_CS_MASK) | KM_TCC3_STOP;
}

void kmTimer3Restart(void) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		TCNT3 = KMC_UNSIGNED_ZERO;
	}
}

void kmTimer3ConfigureOCA(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC3_A_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer3OtputComparePinUsedA) {
		return;
	}
	
	KM_TCC3_PWM_DDR |= KM_TCC3_PWM_BV_A;		// setup port for output
	KM_TCC3_PWM_PORT |= KM_TCC3_PWM_BV_A;		// HIGH by default

	TCCR3A = (TCCR3A & ~KM_TCC3_COMP_A_MASK) | compareOutputMode;

	_Timer3OtputComparePinUsedA = true;
}

void kmTimer3ConfigureOCB(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC3_B_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer3OtputComparePinUsedB) {
		return;
	}
	KM_TCC3_PWM_DDR |= KM_TCC3_PWM_BV_B;		// setup port for output
	KM_TCC3_PWM_PORT |= KM_TCC3_PWM_BV_B;		// HIGH by default

	TCCR3A = (TCCR3A & ~KM_TCC3_COMP_B_MASK) | compareOutputMode;

	_Timer3OtputComparePinUsedB = true;
}

#ifdef COM3C0
void kmTimer3ConfigureOCC(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC3_C_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer3OtputComparePinUsedC) {
		return;
	}
	KM_TCC3_PWM_DDR |= KM_TCC3_PWM_BV_C;		// setup port for output
	KM_TCC3_PWM_PORT |= KM_TCC3_PWM_BV_C;		// HIGH by default

	TCCR3A = (TCCR3A & ~KM_TCC3_COMP_C_MASK) | compareOutputMode;

	_Timer3OtputComparePinUsedC = true;
}
#endif /* COM3C0 */

void kmTimer3SetValueOvf(uint16_t value) {
	ICR3 = _Timer3CompOvfCycles = value; // -V2561
}

void kmTimer3SetValueCompA(uint16_t value) {
	OCR3A = _Timer3CompACycles = value; // -V2561
}

void kmTimer3SetValueCompB(uint16_t value) {
	OCR3B = _Timer3CompBCycles = value; // -V2561
}

#ifdef COM3C0
void kmTimer3SetValueCompC(uint16_t value) {
	OCR3C = _Timer3CompCCycles = value; // -V2561
}
#endif /* COM3C0 */

uint16_t kmTimer3GetValueOvf(void) {
	return _Timer3CompOvfCycles;
}

uint16_t kmTimer3GetValueCompA(void) {
	return _Timer3CompACycles;
}

uint16_t kmTimer3GetValueCompB(void) {
	return _Timer3CompBCycles;
}

#ifdef COM3C0
uint16_t kmTimer3GetValueCompC(void) {
	return _Timer3CompCCycles;
}
#endif /* COM3C0 */

void kmTimer3SetPrescale(uint8_t prescaler) {
	_Timer3PrescalerSelectBits = prescaler;
	kmTimer3Start();
}

void kmTimer3EnableInterruptCompA(void) {
	TIMSK3 |= _BV(OCIE3A);
}

void kmTimer3EnableInterruptCompB(void) {
	TIMSK3 |= _BV(OCIE3B);
}

#ifdef COM3C0
void kmTimer3EnableInterruptCompC(void) {
	TIMSK3 |= _BV(OCIE3C);
}
#endif /* COM3C0 */

void kmTimer3EnableInterruptOVF(void) {
	TIMSK3 |= _BV(TOIE3);
}

void kmTimer3EnableInterruptInpCapture(void) {
	TIMSK3 |= _BV(ICIE3);
}

void kmTimer3DisableInterruptCompA(void) {
	TIMSK3 &= ~_BV(OCIE3A);
}

void kmTimer3DisableInterruptCompB(void) {
	TIMSK3 &= ~_BV(OCIE3B);
}

#ifdef COM3C0
void kmTimer3DisableInterruptCompC(void) {
	TIMSK3 &= ~_BV(OCIE3C);
}
#endif /* COM3C0 */

void kmTimer3DisableInterruptOVF(void) {
	TIMSK3 &= ~_BV(TOIE3);
}

void kmTimer3DisableInterruptInpCapture(void) {
	TIMSK3 &= ~_BV(ICIE3);
}

void kmTimer3DisableInterruptsAll(void) {
#ifdef COM3C0
	TIMSK3 &= ~(_BV(OCIE3C) | _BV(OCIE3B) | _BV(OCIE3A) | _BV(TOIE3 | _BV(ICIE3)));
#else
	TIMSK3 &= ~(_BV(OCIE3B) | _BV(OCIE3A) | _BV(TOIE3 | _BV(ICIE3)));
#endif /* COM3C0 */
}

void kmTimer3RegisterCallbackCompA(void *userData, kmTimer3CallbackType *callback) {
	_timer3CallbackCompA = callback;
	_Timer3CallbackUserDataCompA = userData;
}

void kmTimer3RegisterCallbackCompB(void *userData, kmTimer3CallbackType *callback) {
	_timer3CallbackCompB = callback;
	_Timer3CallbackUserDataCompB = userData;
}

#ifdef COM3C0
void kmTimer3RegisterCallbackCompC(void *userData, kmTimer3CallbackType *callback) {
	_timer3CallbackCompC = callback;
	_Timer3CallbackUserDataCompC = userData;
}
#endif /* COM3C0 */

void kmTimer3RegisterCallbackOVF(void *userData, kmTimer3CallbackType *callback) {
	_timer3CallbackOVF = callback;
	_Timer3CallbackUserDataOVF = userData;
}

void kmTimer3RegisterCallbackInpCapture(void *userData, kmTimer3InpCaptureCallbackType *callback) {
	_timer3CallbackInpCapture = callback;
	_Timer3CallbackUserDataInpCapture = userData;
}

void kmTimer3UnregisterCallbackCompA(void) {
	_Timer3CallbackUserDataCompA = NULL;
}

void kmTimer3UnregisterCallbackCompB(void) {
	_Timer3CallbackUserDataCompB = NULL;
}

#ifdef COM3C0
void kmTimer3UnregisterCallbackCompC(void) {
	_Timer3CallbackUserDataCompC = NULL;
}
#endif /* COM3C0 */

void kmTimer3UnregisterCallbackOVF(void) {
	_Timer3CallbackUserDataOVF = NULL;
}

void kmTimer3UnregisterCallbackInpCapture(void) {
	_timer3CallbackInpCapture = NULL;
}

void kmTimer3SetCallbackUserDataOVF(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer3CallbackUserDataOVF = userData;
	}
}

void kmTimer3SetCallbackUserDataInpCapture(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer3CallbackUserDataInpCapture = userData;
	}
}

void kmTimer3SetCallbackUserDataCompA(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer3CallbackUserDataCompA = userData;
	}
}

void kmTimer3SetCallbackUserDataCompB(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer3CallbackUserDataCompB = userData;
	}
}

#ifdef COM3C0
void kmTimer3SetCallbackUserDataCompC(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer3CallbackUserDataCompC = userData;
	}
}
#endif /* COM3C0 */

bool kmTimer3InpCaputureGetState(void) {
	return (KM_TCC3_ICP3_PORT_INPUT & _BV(KM_TCC3_ICP3_PIN));
}

ISR(TIMER3_COMPA_vect) {
	if (NULL != _timer3CallbackCompA) {
		_timer3CallbackCompA(_Timer3CallbackUserDataCompA);
	}
}

ISR(TIMER3_COMPB_vect) {
	if (NULL != _timer3CallbackCompB) {
		_timer3CallbackCompB(_Timer3CallbackUserDataCompB);
	}
}

#ifdef COM3C0
ISR(TIMER3_COMPC_vect) {
	if (NULL != _timer3CallbackCompC) {
		_timer3CallbackCompC(_Timer3CallbackUserDataCompC);
	}
}
#endif /* COM3C0 */

ISR(TIMER3_OVF_vect) {
	if (NULL != _timer3CallbackOVF) {
		_timer3CallbackOVF(_Timer3CallbackUserDataOVF);
	}
}

ISR(TIMER3_CAPT_vect) {
	_inpCaptureCycles = ICR3;
	TCCR3B ^= _BV(ICES3);
	kmTimer3Restart();
	if (NULL != _timer3CallbackInpCapture) {
		_timer3CallbackInpCapture(_inpCaptureCycles, kmTimer3InpCaputureGetState(), _Timer3CallbackUserDataInpCapture);
	}
}

#endif /* TIMSK3 */
