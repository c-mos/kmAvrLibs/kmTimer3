# ReadMe
# kmTimer3 library for AVR MCUs

The kmTimer3 library provides functions to initialize and control Timer 3 on AVR MCUs. It allows generating square waves with specified duty cycle and frequency, registering callbacks for overflow and comparator interrupts, enabling/disabling interrupts, configuring comparator outputs, controlling timer flow, and setting PWM duty cycles.

## Table of Contents

- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-07-06)

## Overview
This library is designed for older models of AVR MCUs like ATmega8, ATmega32, etc. It initializes Timer 3 to generate square waves using Phase Accurate mode, allowing precise control over duty cycle and frequency. Callback functions can be registered for overflow and comparator interrupts, enabling users to execute custom routines at specific moments within Timer 3 periods.

## Dependencies
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmCpu](https://gitlab.com/c-mos/kmAvrLibs/kmCpu)
  - [kmTimersCommon](https://gitlab.com/c-mos/kmAvrLibs/kmTimersCommon)

## Usage

Getting this library and adding it to own project:

- To add this module to own project as submodule - enter the main directory of the source code and use git command

```bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmTimer3.git kmTimer3
```

- After cloning own application from git repository use following additional git command to get correct revision of submodule:

```bash
git submodule update --init
```

- Include the library in your AVR project.
- Initialize Timer 3 using `kmTimer3InitOnPrescalerBottomToTopPcPwmOCA`.
- Register callback functions for overflow and comparator interrupts using `kmTimer3RegisterCallbackOVF` and `kmTimer3RegisterCallbackCompA`, respectively.
- Enable interrupts using `kmTimer3EnableInterruptOVF` and `kmTimer3EnableInterruptCompA`.
- Configure comparator outputs using `kmTimer3ConfigureOCA`.
- Control timer flow using `kmTimer3Start`, `kmTimer3Stop`, and `kmTimer3Restart`.
- Set PWM duty cycles using `kmTimer3SetPwmDutyBottomToTop` and `kmTimer3SetPwmDutyAccurateTimeModes`.
- Include the library in your AVR project.
- Initialize Timer 3 using `kmTimer3InitOnPrescalerBottomToTopPcPwmOCA`.
- Register callback functions for overflow and comparator interrupts using `kmTimer3RegisterCallbackOVF` and `kmTimerrRegisterCallbackCompA`, respectively.
- Enable interrupts using `kmTimer3EnableInterruptOVF` and `kmTimer3EnableInterruptCompA`.
- Configure comparator outputs using `kmTimer3ConfigureOCA`.
- Control timer flow using `kmTimer3Start`, `kmTimer3Stop`, and `kmTimer3Restart`.
- Set PWM duty cycles using `kmTimer3SetPwmDutyBottomToTop` and `kmTimer3SetPwmDutyAccurateTimeModes`.

## Example Code

```c
#define KM_TIMER3_TEST_USER_DATA_A 1UL
#define KM_TIMER3_TEST_USER_DATA_B 65535UL
#define KM_TIMER3_TEST_USER_DATA_C 255UL
#define KM_TIMER3_TEST_DUTY_0_PERC KM_TIMER3_BOTTOM
#define KM_TIMER3_TEST_DUTY_25_PERC KM_TIMER3_MID - (KM_TIMER3_MID >> 1)
#define KM_TIMER3_TEST_DUTY_50_PERC KM_TIMER3_MID
#define KM_TIMER3_TEST_DUTY_75_PERC KM_TIMER3_MID + (KM_TIMER3_MID >> 1)
#define KM_TIMER3_TEST_DUTY_100_PERC KM_TIMER3_TOP

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer3/kmTimer3.h"

void callbackOVF(void *userData) {
    dbToggle(DB_PIN_0);
    dbOn(DB_PIN_1);
    dbOn(DB_PIN_2);
}

void callbackCompAOff(void *userData) {
    dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
    dbOff(DB_PIN_2);
}

int main(void) {
    appInitDebug();
    kmCpuInterruptsEnable();

    kmTimer3InitOnPrescalerBottomToTopOvfCompABInterruptCallback(KM_TCC3_PRSC_8);

    kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackOVF);
    kmTimer3EnableInterruptOVF();

    kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAOff);
    kmTimer3SetValueCompA(KM_TIMER3_TEST_DUTY_25_PERC);
    kmTimer3EnableInterruptCompA();

    kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompBOff);
    kmTimer3SetValueCompB(KM_TIMER3_TEST_DUTY_75_PERC);
    kmTimer3EnableInterruptCompB();

    kmTimer3Start();

    while(true) {
    }
}
```

See Also:
[kmTimer3 Example Test Application](https://gitlab.com/c-mos/kmAvrTests/kmTimer3Test)

## Author and License
**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**License**: GPL-3.0-or-later

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)
