/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief Timer3 library functions for AVR MCUs.
* - kmTimer3.h
* - kmTimer3DefaultConfig.h
*
*  **Created on**: Dec 10, 2023 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmDebug library for AVR MCUs @n
*  **Copyright (C) 2022  Krzysztof Moskwa**
*  
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef KM_TIMER_3_H_
#define KM_TIMER_3_H_

#ifdef KM_DOXYGEN
#define TIMSK3
#define COM3C0
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif
#ifdef TIMSK3

#include <stdbool.h>

#include "../kmCommon/kmCommon.h"
#include "../kmTimersCommon/kmTimerDefs.h"

/// Remapping macro for saving own user data in callback registration #timer3RegisterCallbackCompA function
#define KM_TIMER3_USER_DATA(X) (void *)(X)

/**
Definition of the Timer3 Callback Type
@param Pointer for void user data content that is used in #timer3RegisterCallbackCompA function
and will be delivered to callback."as is"
*/
typedef void kmTimer3CallbackType(void *);

/**
@brief Callback type for Timer 3 input capture events.

This is the function pointer type for the callback function that will be
called when a Timer 3 input capture event occurs.

@param[out] captureValue The captured value from the Timer 3 input capture.
@param[out] captureState The input state information obtained during the capture event.
@param[out] userData A pointer to user-defined data that was registered with the callback.

@note The callback function must adhere to this signature to be compatible
      with the Timer 3 input capture system.
*/
typedef void kmTimer3InpCaptureCallbackType(const uint16_t, const bool, const void *);

/**
Enumeration type definition of the PWM outputs for timer used in parameters of #kmTimer3SetPwmDuty, #kmTimer3SetPwmDuty, #kmTimer3SetPwmInversion functions .
*/
typedef enum {
	/// PWM on output A.
	KM_TCC3_PWM_OUT_A,
	/// PWM on output B.
	KM_TCC3_PWM_OUT_B,
#ifdef COM3C0
	/// PWM on output C.
	KM_TCC3_PWM_OUT_C
#endif /* COM3C0 */
} Tcc3PwmOut;

/**
 * @brief Initializes Timer 3 in a mode allowing the use of CompA, CompB, CompC, and Overflow Interrupts launched at a specific phase of the timer.
 *
 * The timer always counts from 0 to KM_\b Timer3_MAX value (255). Callbacks for CompA, CompB, and CompC are launched
 * once the timer reaches specific values set by #Timer3SetValueCompA, #Timer3SetValueCompB, and #Timer3SetValueCompC.
 * The callbacks need to be configured with #kmTimer3RegisterCallbackOVF, #kmTimer3RegisterCallbackCompA,
 * #kmTimer3RegisterCallbackCompB, and #kmTimer3RegisterCallbackCompC functions. The period/frequency of each of these Interrupts
 * will be exactly the same and defined by MCU main clock and the prescaler.
 * The Overflow callback is called when Timer 3 reaches the value \b KM_TIMER3_MAX (255).
 * CompA, CompB, and CompC callbacks are called when Timer 3 reaches values set by #Timer3SetValueCompA, #Timer3SetValueCompB, and #Timer3SetValueCompC respectively.
 * Setting these values allows shifting callback calls with phase (with accuracy limited by the time that the MCU takes to execute code within the callback).
 *
 * @param prescaler Selects one of the prescalers as defined in kmTimersCommon/kmTimer3Defs.h (e.g., \b KM_TCC3_PRSC_256).
 *
 * @code
 * #define KM_TIMER3_TEST_USER_DATA_A 1UL
 * #define KM_TIMER3_TEST_USER_DATA_B 65535UL
 * #define KM_TIMER3_TEST_USER_DATA_C 255UL
 * #define KM_TIMER3_TEST_USER_DATA_D 127UL
 * #define KM_TIMER3_TEST_DUTY_0_PERC KM_TIMER3_BOTTOM
 * #define KM_TIMER3_TEST_DUTY_25_PERC KM_TIMER3_MID - (KM_TIMER3_MID >> 1)
 * #define KM_TIMER3_TEST_DUTY_50_PERC KM_TIMER3_MID
 * #define KM_TIMER3_TEST_DUTY_75_PERC KM_TIMER3_MID + (KM_TIMER3_MID >> 1)
 * #define KM_TIMER3_TEST_DUTY_100_PERC KM_TIMER3_TOP
 *
 * #include "kmCpu/kmCpu.h"
 * #include "kmDebug/kmDebug.h"
 * #include "kmTimersCommon/kmTimerDefs.h"
 * #include "kmTimer3/kmTimer3.h"
 *
 * void callbackOVF(void *userData) {
 *     dbToggle(DB_PIN_0);
 *     dbOn(DB_PIN_1);
 *     dbOn(DB_PIN_2);
 *     dbOn(DB_PIN_3);
 * }
 *
 * void callbackCompAOff(void *userData) {
 *     dbOff(DB_PIN_1);
 * }
 *
 * void callbackCompBOff(void *userData) {
 *     dbOff(DB_PIN_2);
 * }
 *
 * void callbackCompCOff(void *userData) {
 *     dbOff(DB_PIN_3);
 * }
 *
 * int main(void) {
 *     appInitDebug();
 *     kmCpuInterruptsEnable();
 *
 *     kmTimer3InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC3_PRSC_8);
 *
 *     kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackOVF);
 *     kmTimer3EnableInterruptOVF();
 *
 *     kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAOff);
 *     kmTimer3SetValueCompA(KM_TIMER3_TEST_DUTY_25_PERC);
 *     kmTimer3EnableInterruptCompA();
 *
 *     kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompBOff);
 *     kmTimer3SetValueCompB(KM_TIMER3_TEST_DUTY_75_PERC);
 *     kmTimer3EnableInterruptCompB();
 *
 *     kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackCompCOff);
 *     kmTimer3SetValueCompC(KM_TIMER3_TEST_DUTY_50_PERC);
 *     kmTimer3EnableInterruptCompC();
 *
 *     kmTimer3Start();
 *
 *     while(true) {
 *     }
 * @endcode
 *
 * Output from the example code:
 * \image html kmTimer3Test2.png
 */
void kmTimer3InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);

/**
Initializes Timer 3 in the mode allowing to generate frequency with specific period on OCA0 output.
This function tries to configure Timer3 to make the period as accurate as possible -
see limitations in the description of #kmTimer3CalcPerdiod function.
To change period while Timer3 is running calculate new CompA value and use #kmTimer3SetValueCompA and #kmTime0SetPrescale functions.
with kmTimer3CalcPerdiod shifting new periodInMicroseconds by one bit to the right
@param periodInMicroseconds period of the square function on the OCA0, the available range depends on the main MCU clock

Example code:
@code
	kmTimer3InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER3_TEST_1MS);
	kmTimer3Start();
@endcode
Output from the example code:
\image html kmTimer3Test0.png
*/
void kmTimer3InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds);

/**
Initializes Timer 3 in the mode allowing to use CompA Interrupt launched every specific period defined in microseconds.
The callback needs to be configured with #kmTimer3RegisterCallbackCompA function.
It's also possible to generate the square wave toggling output with the same period (output frequency is half of frequency of executing callbacks)
Note accuracy is limited with time that used by MCU to execute code within callback.
@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock

@code
	
(KM_TIMER3_TEST_5MS, true);

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

	kmTimer3Start();
}
@endcode
Output from the example code:
\image html kmTimer3Test1.png
*/
void kmTimer3InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC3A);

/**
Initializes Timer 3 as a Counter of either rising or falling signal edges on the input T0
@param falling if true the counter will count falling edges, if false - rising edges
*/
void kmTimer3InitExternal(bool falling);

/**
Initializes Timer 3 in the mode allowing to use CompA and CompB Interrupt launched every specific period defined in microseconds.
The CompB Interrupt can be shifted in phase by providing corresponding phaseIntB parameter
The callbacks needs to be configured with #kmTimer3RegisterCallbackCompA and #kmTimer3RegisterCallbackCompB functions.
The timer goes always from 0 to top value defined as calculation for microseconds used as parameter.
This function tries to define period of calling the callbacks as accurate as possible
The range of available frequencies depends on used XTAL\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock
@param phaseIntB phase shift between executing CompA and CompB callbacks. The range of the value from 0 to 255, where 0 means no phase shift and 128 means 180 phase shift.
The accuracy of the phase shift depends on on the main MCU clock and the microseconds parameter

@code
	kmTimer3InitOnAccurateTimeCompABInterruptCallback(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_PHASE_180_DEG); // for 1 millisecond

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();
	kmTimer3Start();
@endcode
Output from the example code:
\image html kmTimer3Test5.png
*/
void kmTimer3InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);

/**
Initializes Timer 3 to generate square wave on the OC3x pin of the MCU with specified time period and duty cycle (Fast PWM mode)
To change the duty cycle after initialization #kmTimer3SetPwmDuty function can be used
(only #KM_TCC3_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC3_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 3 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC3x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC3A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC3A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC3A pin is inverted
@param dutyB duty of the square wave generated on the OC3B pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC3B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC3B pin is inverted
@param dutyC Duty of the square wave generated on the OC3C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC3C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimeFastPwm(KM_Timer3_TEST_1MS, true, KM_Timer3_TEST_DUTY_25_PERC, false);

	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();

	kmTimer3Start();

	// to change duty on the channel B
	testSwipePwm(KM_TCC3_PWM_OUT_B, cyclesRange);

#endif	
@endcode
Output from the example code:
\image html kmTimer3Test5.png
*/

#ifdef COM3C0
uint16_t kmTimer3InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM3C0 */
uint16_t kmTimer3InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM3C0 */

/**
Initializes Timer 3 to generate square wave on the OC3x pin of the MCU with specified time period and duty cycle (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer3SetPwmDuty function can be used
(only #KM_TCC3_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC3_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 3 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC3x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC3A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC3A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC3A pin is inverted
@param dutyB duty of the square wave generated on the OC3B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC3B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC3B pin is inverted
@param dutyC Duty of the square wave generated on the OC3C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC3C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimePcPwm(KM_TIMER3_TEST_1MS, true, KM_Timer3_TEST_DUTY_25_PERC, false);
	kmTimer3Start();

	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();

	testSwipePwm(KM_TCC3_PWM_OUT_B, KM_TIMER3_MAX);
@endcode
Output from the example code:
\image html kmTimer3Test7.png
*/
#ifdef COM3C0
uint16_t kmTimer3InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM3C0 */
uint16_t kmTimer3InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM3C0 */

/**
Initializes Timer 3 to generate square wave on the OC3x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer3SetPwmDuty function can be used (for both #KM_TCC3_PWM_OUT_A and #KM_TCC3_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 3 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer3Defs.h (e.g. KM_TCC3_PRSC_256)
@param dutyA duty of the square wave generated on the OC3A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC3A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC3A pin is inverted
@param dutyB duty of the square wave generated on the OC3B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC3B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC3B pin is inverted
@param dutyC Duty of the square wave generated on the OC3C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC3C pin is inverted.

@code
kmTimer3InitOnPrescalerBottomToTopFastPwm(KM_TCC3_PRSC_64, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_75_PERC, false);

kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer3EnableInterruptOVF();

kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer3EnableInterruptCompA();

kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer3EnableInterruptCompB();

kmTimer3Start();

testSwipePwm(KM_TCC3_PWM_OUT_A, KM_TIMER3_MAX);
testSwipePwm(KM_TCC3_PWM_OUT_B, KM_TIMER3_MAX);
@endcode
Output from the example code:
\image html kmTimer3Test4.png
*/
#ifdef COM3C0
void kmTimer3InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM3C0 */
void kmTimer3InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM3C0 */

/**
Initializes Timer 3 to generate square wave on the OC3x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer3SetPwmDuty function can be used (for both #KM_TCC3_PWM_OUT_A and #KM_TCC3_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 3 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer3Defs.h (e.g. KM_TCC3_PRSC_256)
@param dutyA duty of the square wave generated on the OC3A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC3A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC3A pin is inverted
@param dutyB duty of the square wave generated on the OC3B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC3B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC3B pin is inverted
@param dutyC Duty of the square wave generated on the OC3C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC3C pin is inverted.

@code
kmTimer3InitOnPrescalerBottomToTopPcPwm(KM_TCC3_PRSC_64, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_75_PERC, false);

kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer3EnableInterruptOVF();

kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer3EnableInterruptCompA();

kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer3EnableInterruptCompB();

kmTimer3Start();

testSwipePwm(KM_TCC3_PWM_OUT_A, KM_TIMER3_MAX);
testSwipePwm(KM_TCC3_PWM_OUT_B, KM_TIMER3_MAX);
@endcode
Output from the example code:
\image html kmTimer3Test6.png
*/
#ifdef COM3C0
void kmTimer3InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM3C0 */
void kmTimer3InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM3C0 */

/**
 * @brief Initializes Timer/Counter 3 for input capture with specified parameters.
 *
 * This function initializes Timer/Counter 3 for input capture mode, configuring it
 * with the specified parameters. It sets up the necessary callbacks, enables interrupts,
 * and starts the timer for accurate timing measurements.
 * The iddle time defines maximum time of captured input state change.
 *
 * @param iddleTimeMicroseconds The idle time (in microseconds) before capturing signals.
 * @param iddleUserData User data to be passed to the idle time callback function.
 * @param iddleCallback Pointer to the callback function for the idle time interrupt.
 * @param inpCaptureUserData User data to be passed to the input capture callback function.
 * @param inpCaptureCallback Pointer to the callback function for the input capture interrupt.
 */
void kmTimer3InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer3CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer3InpCaptureCallbackType *inpCaptureCallback);

// Setters callback user data
/**
Sets or changes User Data passed to the callback registered with #kmTimer3RegisterCallbackOVF.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer3SetCallbackUserDataOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C));
@endcode
*/
void kmTimer3SetCallbackUserDataOVF(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer3RegisterCallbackCompA.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A));
@endcode
*/
void kmTimer3SetCallbackUserDataCompA(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer3RegisterCallbackCompB.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B));
@endcode
*/
void kmTimer3SetCallbackUserDataCompB(void *userData);

#ifdef COM3C0
/**
Sets or changes User Data passed to the callback registered with #kmTimer3RegisterCallbackCompC.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B));
@endcode
*/
void kmTimer3SetCallbackUserDataCompC(void *userData);
#endif /* COM3C0 */


// Registering callbacks
/**
Functions to register callback launched on Timer3 Overflow. The callback needs to be declared with #kmTimer3CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer3CallbackType type

@code
void callbackOVFToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackOVFToggle);
@endcode
*/
void kmTimer3RegisterCallbackOVF(void *userData, kmTimer3CallbackType *callback);

/**
Functions to register callback launched on Timer3 Overflow Interrupt. The callback needs to be declared with #kmTimer3CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer3CallbackType type

@code
void callbackCompAToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
@endcode
*/
void kmTimer3RegisterCallbackCompA(void *userData, kmTimer3CallbackType *callback);

/**
Functions to register callback launched on Timer3 Overflow Interrupt. The callback needs to be declared with #kmTimer3CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer3CallbackType type

@code
void callbackCompBToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
@endcode
*/
void kmTimer3RegisterCallbackCompB(void *userData, kmTimer3CallbackType *callback);

#ifdef COM3C0
/**
Functions to register callback launched on Timer3 Overflow Interrupt. The callback needs to be declared with #kmTimer3CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER3_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer3CallbackType type

@code
void callbackCompCToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_2);
}

/// application routine
kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCToggle);
@endcode
*/
void kmTimer3RegisterCallbackCompC(void *userData, kmTimer3CallbackType *callback);
#endif /* COM3C0 */

/**
@brief Register a callback function for Timer 3 input capture event.

This function registers a user-provided callback function that will be
called when a Timer 3 input capture event occurs. The user can also
provide a custom data pointer which will be passed back to the callback
when it is invoked.

@param[in] userData A pointer to user-defined data that will be passed to
                    the callback function.
@param[in] callback A pointer to the function that will be called on Timer 3
                    input capture event. This function must match the
                    signature defined by kmTimer5InpCaptureCallbackType.

@note The callback function must be set up before enabling the Timer 3 input
      capture to ensure that events are handled correctly.
*/
void kmTimer3RegisterCallbackInpCapture(void *userData, kmTimer3InpCaptureCallbackType *callback);

// Unregistering callbacks
/**
Permanently unregisters callback registered with #kmTimer3RegisterCallbackOVF function
*/
void kmTimer3UnregisterCallbackOVF(void);

/**
Permanently unregisters callback registered with #kmTimer3RegisterCallbackCompA function
*/
void kmTimer3UnregisterCallbackCompA(void);

/**
Permanently unregisters callback registered with #kmTimer3RegisterCallbackCompB function.
*/
void kmTimer3UnregisterCallbackCompB(void);

#ifdef COM3C0
/**
Permanently unregisters callback registered with #kmTimer3RegisterCallbackCompC function.
*/
void kmTimer3UnregisterCallbackCompC(void);
#endif /* COM3C0 */

/**
Permanently unregisters callback registered with #kmTimer3RegisterCallbackInpCapture function.
*/
void kmTimer3UnregisterCallbackInpCapture(void);

// Enabling interrupts
/**
Enables Timer3 Overflow interrupt allowing to execute routine registered with #kmTimer3RegisterCallbackOVF
*/
void kmTimer3EnableInterruptOVF(void);

/**
Enables Timer3 Comparator A interrupt allowing to execute routine registered with #kmTimer3RegisterCallbackCompA
*/
void kmTimer3EnableInterruptCompA(void);

/**
Enables Timer3 Comparator B interrupt allowing to execute routine registered with #kmTimer3RegisterCallbackCompB
*/
void kmTimer3EnableInterruptCompB(void);

#ifdef COM3C0
/**
Enables Timer3 Comparator C interrupt allowing to execute routine registered with #kmTimer3RegisterCallbackCompC
*/
void kmTimer3EnableInterruptCompC(void);
#endif /* COM3C0 */

/**
 * @brief Enables Timer/Counter 3 Input Capture Interrupt.
 *
 * This function sets the Input Capture Interrupt Enable bit for Timer/Counter 3.
 * After calling this function, the timer will generate an interrupt when an input capture event occurs.
 * The specific input capture event conditions are determined by the timer configuration and mode.
 * Ensure that the corresponding input capture callback is registered using #kmTimer3RegisterCallbackInpCapture
 * before enabling the input capture interrupt.
 *
 * Example usage:
 * @code
 * kmTimer3EnableInterruptInpCapture();
 * @endcode
 */
void kmTimer3EnableInterruptInpCapture(void);

// Disabling interrupts
/**
Disables all Timer 3 interrupts (Overflow, ComparatorA, ComparatorB, ComparatorC & Input Capture).
*/
void kmTimer3DisableInterruptsAll(void);

/**
Disables Timer 3 Overflow interrupt
*/
void kmTimer3DisableInterruptOVF(void);

/**
Disables Timer 3 ComparatorA interrupt
*/
void kmTimer3DisableInterruptCompA(void);

/**
Disables Timer 3 ComparatorB interrupt
*/
void kmTimer3DisableInterruptCompB(void);

#ifdef COM3C0
/**
Disables Timer 3 ComparatorC interrupt
*/
void kmTimer3DisableInterruptCompC(void);
#endif /* COM3C0 */

/**
 * @brief Sets the value for Timer/Counter 3 Overflow.
 *
 * This function sets the value for Timer/Counter 3 Overflow. The timer counts from 0 to this value
 * before generating an overflow interrupt and resetting to zero. Ensure that the corresponding
 * overflow callback is registered using #kmTimer3RegisterCallbackOVF before setting the overflow value.
 *
 * @param value The value to set for Timer/Counter 3 Overflow.
 *
 * Example usage:
 * @code
 * kmTimer3SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
 * @endcode
 */
void kmTimer3SetValueOvf(uint16_t value);

// Configuration of Comparator Outputs
/**
Configures Compare Output Mode channel A for Timer3 (\b OC3A output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer3Defs.h (e.g. \b KM_TCC3_A_COMP_OUT_TOGGLE)
*/
void kmTimer3ConfigureOCA(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel B for Timer3 (\b OC3B output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer3Defs.h (e.g. \b KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer3ConfigureOCB(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel C for Timer3 (\b OC3C output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in
kmTimersCommon/kmTimer3Defs.h (e.g. \b KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer3ConfigureOCC(uint8_t compareOutputMode);

// Controlling timer flow
/**
Enables running of Timer 3, allowing to execute Timer/Counter function defined with Initialization functions
*/
void kmTimer3Start(void);

/**
Stops running of Timer 3
*/
void kmTimer3Stop(void);

/**
Restarts internal counter of Timer 3.
*/
void kmTimer3Restart(void);

// Controlling PWM
/**
Sets the Duty in the PWM modes initialized as Bottom-To-Top. Can be applied to both OC3A and OC3B outputs
@param pwmOut either KM_TCC3_PWM_OUT_A for \b OC3A or KM_TCC3_PWM_OUT_B for \b OC3B
@param duty duty of the square wave generated on the OC3A/OC3B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle
*/

void kmTimer3SetPwmDutyBottomToTop(Tcc3PwmOut pwmOut, uint16_t duty);
/**
Sets the Duty in the PWM modes initialized as Accurate-Time. Can be applied to OC3B output only (as Comparator A is used to control general time period)
@param duty duty of the square wave generated on the OC3A/OC3B pin of the MCU; the range from 0 to 255,
where 64 means 25%/75% and 128 means 50%/50% duty cycle; this initialization offers maximum accuracy of the duty cycle
The accuracy of the value depends on the main clock of the MCU and \e microseconds parameter provided in initialization function
The function calculates it in the way to be at least equal or higher than #KM_TCC3_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer3DefaultConfig
*/
void kmTimer3SetPwmDutyAccurateTimeModes(uint16_t duty);

/**
Function allows to invert PWM output either on \b OC3A or on \b OC3B
@param pwmOut either KM_TCC3_PWM_OUT_A for \b OC3A or KM_TCC3_PWM_OUT_B for \b B
@param inverted value true inverts the PWM output, normal operation when falseOC3
*/
void kmTimer3SetPwmInversion(Tcc3PwmOut pwmOut, bool inverted);

// Timer flow calculations
/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period\n

@param microseconds time period of the expected Comparator matches for Timer 3
the available range and accuracy depends on the main MCU clock as specified in the table below
\b NOTE: Some modes of Timer3 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.\n
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer3Defs.h to be used in #kmTimer3Init
@return comparator match value to be used in Timer initialization #kmTimer3Init function
*/
uint16_t kmTimer3CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);

/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period. This version takes into consideration minim resolution of PWM duty and decreases frequency if needed\n
@param microseconds time period of the expected Comparator matches for Timer 3
the available range and accuracy depends on the main MCU clock as specified in the table below.
Note this version of the function takes into consideration desired minimum resolution of the output square wave duty.
In practice this may only matter for very low main clock frequencies. If the desired minimum resolution cannot be reached,
this function will return prescaler and comparator match value decreasing desired period defined in milliseconds
\b NOTE: Some modes of Timer3 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer3Defs.h to be used in #kmTimer3Init
@param minimumCycleAcuracy desired minimum resolution of the output square wave duty (e.g. 4 means that at least 0%, 25%, 50% and 75% are achievable)
@return comparator match value to be used in Timer initialization #kmTimer3Init function
*/
uint16_t kmTimer3CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);

/**
Calculates compare match result based on desired square wave duty and available compare match cycles
calculated for Accurate-Time modes (e.g. got as result of #kmTimer3CalcPerdiod or #kmTimer3InitOnAccurateTimeFastPwm)
@param duty desired duty cycle
@param cyclesRange available range of cycles for duty cycle
@return comparator match value corresponding to desired duty cycle (can be used e.g. in #kmTimer3SetValueCompA)
*/
uint16_t kmTimer3CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange);


/**
 * @brief Retrieves the current value of Timer/Counter 3 Overflow.
 *
 * This function returns the current value of Timer/Counter 3 Overflow, representing the number of cycles
 * before the timer overflows and triggers an overflow interrupt.
 *
 * @return The current value of Timer/Counter 3 Overflow.
 *
 * Example usage:
 * @code
 * uint16_t ovfValue = kmTimer3GetValueOvf();
 * @endcode
 */
uint16_t kmTimer3GetValueOvf(void);

/**
 * @brief Retrieves the current value of Timer/Counter 3 Compare A.
 *
 * This function returns the current value of Timer/Counter 3 Compare A, representing the number of cycles
 * before the timer triggers a Compare A interrupt.
 *
 * @return The current value of Timer/Counter 3 Compare A.
 *
 * Example usage:
 * @code
 * uint16_t compAValue = kmTimer3GetValueCompA();
 * @endcode
 */
uint16_t kmTimer3GetValueCompA(void);

/**
 * @brief Retrieves the current value of Timer/Counter 3 Compare B.
 *
 * This function returns the current value of Timer/Counter 3 Compare B, representing the number of cycles
 * before the timer triggers a Compare B interrupt.
 *
 * @return The current value of Timer/Counter 3 Compare B.
 *
 * Example usage:
 * @code
 * uint16_t compBValue = kmTimer3GetValueCompB();
 * @endcode
 */
uint16_t kmTimer3GetValueCompB(void);

#ifdef COM3C0
/**
 * @brief Retrieves the current value of Timer/Counter 3 Compare C.
 *
 * This function returns the current value of Timer/Counter 3 Compare C, representing the number of cycles
 * before the timer triggers a Compare C interrupt.
 *
 * @return The current value of Timer/Counter 3 Compare C.
 *
 * Example usage:
 * @code
 * uint16_t compCValue = kmTimer3GetValueCompC();
 * @endcode
 */
uint16_t kmTimer3GetValueCompC(void);
#endif /* COM3C0 */

/**
 * @brief Sets the value for Timer/Counter 3 Overflow.
 *
 * This function sets the value for Timer/Counter 3 Overflow. The timer counts from 0 to this value
 * before generating an overflow interrupt and resetting to zero. The value is also stored internally
 * for future reference.
 *
 * @param value The value to set for Timer/Counter 3 Overflow.
 *
 * Example usage:
 * @code
 * kmTimer3SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
 * @endcode
 */
void kmTimer3SetValueOvf(uint16_t value);

/**
 * @brief Sets the value for Timer/Counter 3 Compare A.
 *
 * This function sets the value for Timer/Counter 3 Compare A. The timer counts from 0 to this value
 * before triggering a Compare A interrupt. The value is also stored internally for future reference.
 *
 * @param value The value to set for Timer/Counter 3 Compare A.
 *
 * Example usage:
 * @code
 * kmTimer3SetValueCompA(5000); // Set Compare A value to 5000
 * @endcode
 */
void kmTimer3SetValueCompA(uint16_t value);

/**
 * @brief Sets the value for Timer/Counter 3 Compare B.
 *
 * This function sets the value for Timer/Counter 3 Compare B. The timer counts from 0 to this value
 * before triggering a Compare B interrupt. The value is also stored internally for future reference.
 *
 * @param value The value to set for Timer/Counter 3 Compare B.
 *
 * Example usage:
 * @code
 * kmTimer3SetValueCompB(3000); // Set Compare B value to 3000
 * @endcode
 */
void kmTimer3SetValueCompB(uint16_t value);

#ifdef COM3C0
/**
 * @brief Sets the value for Timer/Counter 3 Compare C.
 *
 * This function sets the value for Timer/Counter 3 Compare C. The timer counts from 0 to this value
 * before triggering a Compare C interrupt. The value is also stored internally for future reference.
 *
 * @param value The value to set for Timer/Counter 3 Compare C.
 *
 * Example usage:
 * @code
 * kmTimer3SetValueCompC(1000); // Set Compare C value to 1000
 * @endcode
 */
void kmTimer3SetValueCompC(uint16_t value);
#endif /* COM3C0 */

/**
 * @brief Gets the state of Timer/Counter 3 Input Capture pin.
 *
 * This function returns the current state of the Timer/Counter 3 Input Capture pin. 
 * It checks whether the Input Capture pin is currently high or low.
 *
 * @return The state of the Timer/Counter 3 Input Capture pin:
 *         - \c true if the Input Capture pin is high.
 *         - \c false if the Input Capture pin is low.
 *
 * Example usage:
 * @code
 * bool inpCaptureState = kmTimer3InpCaputureGetState();
 * if (inpCaptureState) {
 *     // Input Capture pin is high
 * } else {
 *     // Input Capture pin is low
 * }
 * @endcode
 */
bool kmTimer3InpCaputureGetState(void);

/**
 * @brief Initializes Timer 3 with the specified prescaler and modes.
 * 
 * This function initializes Timer 3 with the specified prescaler and modes.
 * 
 * @param prescaler The prescaler value to be set for Timer 3.
 * @param modeA The mode value to be set for Timer 3 control register A (TCCR3A).
 * @param modeB The mode value to be set for Timer 3 control register B (TCCR3B).
 * 
 * @note This function stops Timer 4 before initializing it.
 * 
 * @see kmTimer3Stop()
 */
void kmTimer3Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);

/**
 * @brief Starts Timer 3 with the previously configured prescaler.
 * 
 * This function starts Timer 3 with the previously configured prescaler.
 */
void kmTimer3Start(void);

/**
 * @brief Stops Timer 3.
 * 
 * This function stops Timer 3.
 */
void kmTimer3Stop(void);

/**
 * @brief Restarts Timer 4 by resetting its counter value to zero.
 * 
 * This function restarts Timer 4 by resetting its counter value to zero.
 */
void kmTimer3Restart(void);

#endif /* TIMSK3 */
#ifdef __cplusplus
}
#endif /*  __cplusplus */
#endif /* KM_TIMER_3_H_ */
